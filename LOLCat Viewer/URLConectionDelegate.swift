//
//  urldelegate.swift
//  LOLCat Viewer
//
//  Created by Wayne Ohmer on 10/22/14.
//  Copyright (c) 2014 Wayne Ohmer. All rights reserved.
//
import Foundation
import UIKit

extension FlickrImageViewController {

    func connection(connection: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        self.loadingImageExpectedBytes = response.expectedContentLength
        self.imageData = NSMutableData()
        self.downloadProgressView.hidden = false
    }
    
    func connection(connection: NSURLConnection!, didReceiveData conData: NSData!) {
        self.imageData!.appendData(conData)
        self.downloadProgressView.progress = Float(Int64(self.imageData!.length)/self.loadingImageExpectedBytes)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        dispatch_async(dispatch_get_main_queue()) {
            self.currentPhoto.largeImage = UIImage(data:self.imageData!)
            self.catImageView.image = UIImage(data:self.imageData!)
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            self.spinner.stopAnimating()
            self.savedImageArray.append(self.currentPhoto)
            self.savedImageIndex = self.savedImageArray.count-1
            self.savedIndexArray.append("\(self.randomImageIndex) of \(self.photoArray.count)")
            self.updateIndexLabel()
            self.downloadProgressView.hidden = true
            self.imageTapGestureRecognizer.enabled = true
        }
    }
    
    func connection(connection: NSURLConnection!,didFailWithError error: NSError){
        dispatch_async(dispatch_get_main_queue()) {
            self.imageTapGestureRecognizer.enabled = true
            NSLog("\(error)")
        }
    }
}

