//
//  FlickrPhoto.swift
//  LCOLcat Viewer
//
//  Created by Wayne Ohmer on 9/23/14.
//  Copyright (c) 2014 Wayne Ohmer. All rights reserved.
//

import Foundation
import UIKit

class FlickrPhoto {
    
    var thumbnail: UIImage?
    var largeImage: UIImage?
    var photoID = ""
    var server = ""
    var farm = Int(0)
    var secret = ""
    var searchTerm = ""
    var title = ""

}

class Flickr {

    let flickAPIkey = "4961c4c171778f54c2d5ff21cb9baecc"

    func flickrSearchURLForSearchTerm(searchTerm:String, withNumberOfResults numberOfResults: Int) -> String {
        let urlString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(flickAPIkey)&text=\(searchTerm)(&per_page=\(numberOfResults)&format=json&nojsoncallback=1"
        return urlString.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
    }
    
    func flickrPhotoURLForFlickrPhoto(photo: FlickrPhoto, size: String) -> String {
        return "https://farm\(photo.farm).staticflickr.com/\(photo.server)/\(photo.photoID)_\(photo.secret)_\(size).jpg"
    }

    func flickSearchWithSearchTerm(searchTerm: String, withNumberOfResults numberOfResults: Int, withCompletionBlock completionBlock:(resultsArray: [FlickrPhoto], error: NSError?) -> Void) {

        let searchURLstring = self.flickrSearchURLForSearchTerm(searchTerm, withNumberOfResults: numberOfResults)

        let urlRequest = NSURLRequest(URL: NSURL(string: searchURLstring)!)

        NSURLConnection.sendAsynchronousRequest(urlRequest, queue: NSOperationQueue.mainQueue()) { (response: NSURLResponse?, data: NSData?, urlError: NSError?) -> Void in

            var photoArray = [FlickrPhoto]()
            if (urlError == nil && data != nil){
                do {
                    let jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options:.AllowFragments) as! [String:AnyObject]
                    if let status = jsonResult["stat"] as? String {
                        if (status == "fail"){
                            let searchError = NSError(domain: "FlickrSearch", code: 0, userInfo:jsonResult["message"] as? [String:AnyObject])
                            print(jsonResult["message"])
                            completionBlock(resultsArray: photoArray,error: searchError)
                        } else {
                            var nextResult = jsonResult["photos"] as! [String:AnyObject]
                            let resultsArray = nextResult["photo"] as! [[String:AnyObject]]
                            if resultsArray.count > 0 {
                                for thisPhoto in resultsArray {
                                    let myFlickrPhoto:FlickrPhoto = FlickrPhoto()
                                    myFlickrPhoto.farm = thisPhoto["farm"]?.integerValue ?? 0
                                    myFlickrPhoto.server = thisPhoto["server"] as? String ?? ""
                                    myFlickrPhoto.secret = thisPhoto["secret"] as? String ?? ""
                                    myFlickrPhoto.photoID = thisPhoto["id"] as? String ?? ""
                                    myFlickrPhoto.title = thisPhoto["title"] as? String ?? ""
                                    myFlickrPhoto.searchTerm = searchTerm
                                    photoArray.append(myFlickrPhoto)
                                }
                            }
                        }
                    }
                } catch let jsonError as NSError {
                    completionBlock(resultsArray: photoArray,error: jsonError)
                }
            }
            completionBlock(resultsArray: photoArray,error: urlError)
        }
    }
}

