//
//  ViewController.swift
//  LCOLcat Viewer
//
//  Created by Wayne Ohmer on 9/23/14.
//  Copyright (c) 2014 Wayne Ohmer. All rights reserved.
//
import Foundation
import UIKit

class FlickrImageViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var catImageView: UIImageView!
    let wilmaImage = UIImage(named: "wilma")!
    let failImage = UIImage(named: "failCat.jpg")!
    var photoArray = [FlickrPhoto]()
    var savedImageArray = [FlickrPhoto]()
    var savedIndexArray = [String]()
    var randomImageIndex = Int(0)
    var savedImageIndex = Int(0)
    var oldSearchString = ""
    var loadingImageConnecion: NSURLConnection?
    var currentPhoto = FlickrPhoto()
    let numberOfSearchresults = Int(500)
    let animationDuration = NSTimeInterval(0.2)
    let animationPixelOffset = CGFloat(20)
    var imageData: NSMutableData?
    var imageProgress = Float(0)
    var loadingImageExpectedBytes = Int64(0)
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var indexLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchStringField: UITextField!
    @IBOutlet weak var catXdefault: NSLayoutConstraint!
    @IBOutlet weak var downloadProgressView: UIProgressView!
    @IBOutlet var imageTapGestureRecognizer: UITapGestureRecognizer!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let myColor:UIColor = UIColor(white: 0, alpha: 1)
        catImageView.layer.borderColor = myColor.CGColor
        catImageView.layer.borderWidth = 2
        self.searchStringField.delegate = self
        self.searchStringField.text = "lolcat"
        self.spinner.stopAnimating()
    }

    @IBAction func catPictureSwipe(recognizer:UISwipeGestureRecognizer) {
        var animateFlag:Bool = false
        switch recognizer.direction {
            
        case UISwipeGestureRecognizerDirection.Left:
            if (self.savedImageIndex < self.savedImageArray.count-1) {
                self.savedImageIndex += 1;
                animateFlag = true
            } else if self.imageTapGestureRecognizer.enabled {
                self.catPictureTouch()
            }
        case UISwipeGestureRecognizerDirection.Right:
            if (self.savedImageIndex > 0) {
                self.savedImageIndex -= 1;
                animateFlag = true
            }
        case UISwipeGestureRecognizerDirection.Up:
            self.savedImageIndex = self.savedImageArray.count - 1
            animateFlag = true
        case UISwipeGestureRecognizerDirection.Down:
            animateFlag = true
            self.savedImageIndex = 0
        default:
            break
        }
        if (self.savedImageIndex >= 0 && self.savedImageIndex < self.savedImageArray.count) {
            if animateFlag {
                let myPhoto = self.savedImageArray[self.savedImageIndex]
                self.animateTransition(recognizer.direction,image:myPhoto.largeImage!)
                self.updateIndexLabel()
            }
        }
    }
    
    func animateTransition(direction:UISwipeGestureRecognizerDirection,image:UIImage) {
        //Not pretty, could break this up with functions to make the nesting more readable.
        if (direction == UISwipeGestureRecognizerDirection.Left || direction == UISwipeGestureRecognizerDirection.Up){
            //animate out
            UIView.animateWithDuration(self.animationDuration, animations: {
                self.catXdefault.constant = self.view.layer.frame.width - self.animationPixelOffset
                self.view.layoutIfNeeded()
                }, completion: {
                    (value: Bool) in
                    //move to other side
                    self.catXdefault.constant = -self.view.layer.frame.width + self.animationPixelOffset
                    self.view.layoutIfNeeded()
                    self.catImageView.image = image
                    //animate back in
                    UIView.animateWithDuration(self.animationDuration, animations: {
                        self.catXdefault.constant = 0
                        self.view.layoutIfNeeded()
                        }, completion: {
                            (value: Bool) in
                    })
            })
        }else if(direction == UISwipeGestureRecognizerDirection.Right || direction == UISwipeGestureRecognizerDirection.Down){
            //animate out
            UIView.animateWithDuration(self.animationDuration, animations: {
                self.catXdefault.constant = -self.view.layer.frame.width + self.animationPixelOffset
                self.view.layoutIfNeeded()
                }, completion: {
                    (value: Bool) in
                    //move to other side
                    self.catXdefault.constant = +self.view.layer.frame.width - self.animationPixelOffset
                    self.view.layoutIfNeeded()
                    self.catImageView.image = image
                    //animate back in
                    UIView.animateWithDuration(self.animationDuration, animations: {
                        self.catXdefault.constant = 0
                        self.view.layoutIfNeeded()
                        }, completion: {
                            (value: Bool) in
                    })
            })
        }
    }

    func updateIndexLabel() {
        let myPhoto:FlickrPhoto = self.savedImageArray[self.savedImageIndex]
        self.indexLabel.text = "\(self.savedIndexArray[self.savedImageIndex]) (\(myPhoto.searchTerm))"
        self.titleLabel.text = "\(myPhoto.title)"
    }
    
    @IBAction func catPictureTouch() {
        self.imageTapGestureRecognizer.enabled = false
        searchFlickrForTerm(self.searchStringField.text!, numberOfResults: self.numberOfSearchresults, saveingThumbnails: true)
    }

    func handleSearchResults(photoArray:[FlickrPhoto], error:NSError?) {

        if (photoArray.count > 0){
            self.photoArray = photoArray
            //sort in random order
            self.photoArray.sortInPlace() { (photo1,photo2) -> Bool in
                return arc4random_uniform(2) == 0
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.getImageForFlickrPhoto(self.photoArray[0])
                self.randomImageIndex = 1
                self.currentPhoto = self.photoArray[0]
            }
        } else {
            dispatch_async(dispatch_get_main_queue()) {
                self.catImageView.image = self.failImage
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.spinner.stopAnimating()
                self.indexLabel.text = "Search Failed"
            }
        }
        self.oldSearchString = self.searchStringField.text!
    }

    func searchFlickrForTerm(searchTerm:String,numberOfResults:Int,saveingThumbnails:Bool) {
        let myFlicker:Flickr = Flickr()
        dispatch_async(dispatch_get_main_queue()) {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            self.spinner.startAnimating()
        }
        if (self.photoArray.count > 0 && self.randomImageIndex < self.photoArray.count && self.searchStringField.text == self.oldSearchString){
            self.getImageForFlickrPhoto(self.photoArray[self.randomImageIndex])
            self.randomImageIndex += 1
        } else {
            self.indexLabel.text = "Searching"
            self.titleLabel.text = ""
            myFlicker.flickSearchWithSearchTerm(searchTerm,withNumberOfResults:numberOfResults,withCompletionBlock:{ (photoArray: [FlickrPhoto], error:NSError?) -> Void in
                self.handleSearchResults(photoArray,error: error)
            })
        }
    }

    func getImageForFlickrPhoto(photo:FlickrPhoto) {
        self.currentPhoto = photo
        let myflicker = Flickr()
        let photoUrlString = myflicker.flickrPhotoURLForFlickrPhoto(photo,size:"b")
        let photoURL = NSURL(string: photoUrlString)!
        let urlRequest = NSURLRequest(URL: photoURL)
        self.loadingImageConnecion = NSURLConnection(request: urlRequest, delegate: self, startImmediately: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
       
        textField.resignFirstResponder()
        self.searchStringField.text = self.searchStringField.text!.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        searchFlickrForTerm(self.searchStringField.text!, numberOfResults: self.numberOfSearchresults, saveingThumbnails: true)
        return true
    }

}